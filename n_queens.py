## The players gonna play on a n * n board. They have the 
## flexibility to start at anywhere they want. 

from pprint import pprint



def n_queens(n, col_idx):
    board = [[0] * n for _ in range(n)]
    place_queen_in_col(board, col_idx, n)

    return board

def place_queen_in_col(board, col_idx, x):
    n = len(board)
    # In order to start from any columns, set the col_idx to n - 1
    # when it reaches the top of the array.(I can also set the 
    # col_idx back to 0 once it reaches the end, but personally 
    # prefer to try the opposite way).
    
    # In this case, We have to set a pointer to store the position, 
    # so that we can stop looping once every column has been looped.
    
    # Different from the four_rooks problem, there is a possibility 
    # that no certain place to set the queen within a column, if the 
    # previous columns are not well-organized. So, before we set the 
    # queen to a certain place, we have to check if it's next column 
    # has a safe place to set the queen with recursion. 
    if x < 1:
        return True
    if col_idx < 0: col_idx = n + col_idx
    for row_idx in range(n):
        if is_safe(board, row_idx, col_idx):
            board[row_idx][col_idx] = 1
            if place_queen_in_col(board, col_idx - 1, x - 1):
              return True
            board[row_idx][col_idx] = 0
    return False



def is_safe(board, row_idx, col_idx):
    n = len(board)
    ## check row
    for i in range(n):
        if board[row_idx][i] == 1 and i != col_idx: return False
    ## check column
    for j in range(n):
        if board[j][col_idx] == 1 and j != row_idx: return False
    col_left_up = col_right_up = col_left_down = col_right_down = col_idx
    row_left_up = row_right_up = row_left_down = row_right_down = row_idx
    ## check left-up diagonal
    while col_left_up > 0 and row_left_up > 0:
        col_left_up -= 1
        row_left_up -= 1
        if board[row_left_up][col_left_up] == 1: return False
    ## check right-down diagonal
    while col_right_down < n - 1 and row_right_down < n - 1:
        col_right_down += 1
        row_right_down += 1
        if board[row_right_down][col_right_down] == 1: return False
    ## check left-down diagonal
    while col_left_down > 0 and row_left_down < n - 1:
        col_left_down -= 1
        row_left_down += 1
        if board[row_left_down][col_left_down] == 1: return False
    ## check right-up diagonal
    while col_right_up < n - 1 and row_right_up > 0:
        row_right_up -= 1
        col_right_up += 1
        if board[row_right_up][col_right_up] == 1: return False
    return True


if __name__ == "__main__":
    board = n_queens()
    pprint(board, width=20)